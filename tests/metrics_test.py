import unittest
from eviml.metrics import accuracy, precision, recall, f1_score


class TestMetrics(unittest.TestCase):

    def test_accuracy(self):
        self.assertEqual(accuracy([0, 1, 2, 0, 1, 2], [0, 2, 1, 0, 0, 1]), 0.3333333333333333)
        self.assertEqual(accuracy([0, 1, 2, 0, 1, 2], [0, 2, 1, 0, 1, 1]), 0.5)

    def test_precision(self):
        self.assertEqual(precision([0, 1, 2, 0, 1, 2], [0, 2, 1, 0, 0, 1]), {0: 0.6666666666666666, 1: 0.0, 2: 0.0})

    def test_recall(self):
        self.assertEqual(recall([0, 1, 2, 0, 1, 2], [0, 2, 1, 0, 0, 1]), {0: 1.0, 1: 0.0, 2: 0.0})

    def test_f1_score(self):
        self.assertEqual(f1_score([0, 1, 2, 0, 1, 2], [0, 2, 1, 0, 0, 1]), {0: 0.8, 1: 0, 2: 0})


if __name__ == '__main__':
    unittest.main()
