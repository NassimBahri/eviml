from eviml.eviar.Fimed import Fimed


class MSFimed(Fimed):
    """
    Multiple Support FIMED.
    Generates Evidential Frequent Itemsets and Association Rules


    Attributes
    ----------
    class_attribute : str
        Class attribute
    class_labels : dict
        List of class labels

    Methods
    -------
    __init__(records, min_support, min_confidence, class_attribute)
        Constructor of the class
    check_class_attribute(rids1, rids2)
        Calculate the intersection between the rids of two itemsets
    generate_itemsets(item1, item2, level)
        Generate a candidate itemset from two frequent itemsets

    Examples
    --------
    >>> np.add(1, 2)
    """

    def __init__(self, records, min_support, min_confidence, class_attribute):
        """Constructor of the class

        Parameters
        ----------
        records : list
            A records iterable object (json format)
        min_support : float
            Minimum support
        min_confidence : float
            Minimum confidence
        class_attribute : str
            Class attribute (example: "3")
        """
        self.class_attribute = class_attribute
        self.class_labels = {}
        for record in records:
            for fe in record[self.class_attribute].keys():
                if fe not in self.class_labels:
                    self.class_labels[fe] = 0
                self.class_labels[fe] += 1
        super().__init__(records, min_support, min_confidence)

    def fim(self):
        """Generates the frequent itemsets from the RidLists structure
        """
        k = 0
        self.frequent_itemsets.append({})
        # Generate frequent items (frequent 1-itemsets)
        for item in self.ridlists.rid:
            support = self.bel(item)
            self.frequent_itemsets[k][item] = support
        # Generate (k+1)-frequent itemsets
        while len(self.frequent_itemsets[k]) > 0:
            frequent = self.frequent_itemsets[k]
            frequent_itemsets_size = len(frequent)
            candidate_itemsets = []
            step1 = 0
            for item1 in frequent:
                step2 = 0
                for item2 in frequent:
                    if step1 <= step2:
                        continue
                    itemset = Fimed.generate_itemsets(item1, item2, k + 2)
                    if itemset != "":
                        candidate_itemsets.append(itemset)
                    step2 += 1
                step1 += 1
            k += 1
            self.frequent_itemsets.append({})
            for candidate in candidate_itemsets:
                if k == 1:
                    if not self.check_class_attribute(candidate):
                        continue
                support = self.bel(candidate) / self.class_labels[self.get_class_label(candidate)]
                if support >= self.min_support:
                    self.frequent_itemsets[k][candidate] = support

    def check_class_attribute(self, itemset):
        """This method checks whether the itemset contains the class attribute

        Parameters
        ----------
        itemset : str
            Candidate itemset

        Returns
        -------
        bool
            True if the itemset contains the class attribute, False otherwise
        """
        items = itemset.split(";")
        for item in items:
            elements = item.split("#")
            if elements[0] == self.class_attribute:
                return True
        return False

    def get_class_label(self, itemset):
        """This method returns the class label of a given itemset

        Parameters
        ----------
        itemset : str
            Itemset

        Returns
        -------
        str
            Class label
        """
        position = itemset.index(self.class_attribute+"#")
        return itemset[position+2:]

    def generate_rules(self):
        """Generates evidential association rules


        Returns
        -------
        list
            List of rules
        """
        if len(self.frequent_itemsets) < 2:
            exit()
        max_level = len(self.frequent_itemsets)
        for level in range(1, max_level):
            frequents = self.frequent_itemsets[level]
            for item, support in frequents.items():
                parts = item.split(";")
                for part in parts:
                    rule_consequent = part
                    antecedent = parts.copy()
                    antecedent.remove(part)
                    rule_antecedent = ";"
                    rule_antecedent = rule_antecedent.join(antecedent)
                    if rule_antecedent in self.frequent_itemsets[len(antecedent) - 1]:
                        support_antecedent = self.frequent_itemsets[len(antecedent) - 1][rule_antecedent]
                    else:
                        support_antecedent = self.bel(rule_antecedent)
                    if rule_consequent in self.frequent_itemsets[0]:
                        support_consequent = self.frequent_itemsets[0][rule_consequent]
                    else:
                        support_consequent = self.bel(rule_antecedent)
                    lift = support / (support_antecedent * support_consequent)
                    confidence = support / support_antecedent
                    if confidence >= self.min_confidence:  # and lift > 1 and per < 0.5:
                        self.rules.append({
                            "antecedent": rule_antecedent,
                            "consequent": rule_consequent,
                            "support": support,
                            "confidence": confidence,
                            "lift": lift
                        })
