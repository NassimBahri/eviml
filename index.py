from eviml.eviar.MSFimed import MSFimed, Fimed
import json
import time
from eviml.metrics import precision, recall, accuracy, f1_score

y_true = [0, 1, 2, 0, 1, 2]
y_pred = [0, 2, 1, 0, 0, 1]
print(precision(y_true, y_pred))
print(recall(y_true, y_pred))
print(accuracy(y_true, y_pred))
print(f1_score(y_true, y_pred))

