"""
.. module:: eviml.edb
"""
import itertools
import json
import random
import math
from os import path
import sys
import traceback


def generate_domain(input, separator):
    attributes = {}
    with open(input) as file:
        while True:
            line = file.readline().strip()
            if not line:
                break
            elements = line.split(separator)
            for i in range(0, len(elements)):
                if str(i + 1) not in attributes:
                    attributes[str(i + 1)] = []
                elem = elements[i]
                if elem not in attributes[str(i + 1)]:
                    attributes[str(i + 1)].append(elem)
    return attributes


def transform(input, output, separator, class_attribute, ignorance=0):
    data = []
    try:
        attributes = generate_domain(input, separator)
        with open(input) as file:
            while True:
                line = {}
                line1 = file.readline().strip()
                if not line1:
                    break
                elements = line1.split(separator)
                for i in range(0, len(elements)):
                    elem = elements[i]
                    fe = str(attributes[str(i + 1)].index(elem) + 1)
                    line[str(i + 1)] = {}
                    vacuous_mass = ignorance
                    if len(attributes[str(i + 1)]) == 1 or i + 1 == class_attribute:
                        vacuous_mass = 0
                    line[str(i + 1)][fe] = 1 - vacuous_mass
                    if vacuous_mass > 0:
                        vacuous = list(range(1, len(attributes[str(i + 1)]) + 1))
                        line[str(i + 1)][",".join(str(x) for x in vacuous)] = vacuous_mass
                data.append(line)
        with open(output, 'w') as file:
            json.dump(data, file)
    except FileNotFoundError:
        print('----------')
        print(f"Error: File not found")
        traceback.print_exc(file=sys.stdout)
        print('----------')


def train_test_split(input_file, class_attribute, train_size, random_state=None):
    try:
        with open(input_file) as file:
            data = json.load(file)
        train_data = []
        test_data = []
        test_labels = []
        if random_state is None:
            random_state = random.randrange(1, 20)
        for i in range(0, random_state):
            random.shuffle(data)
        # Prepare the data set
        prepared_data = {}
        for line in data:
            keys = list(line[class_attribute].keys())
            if keys[0] not in prepared_data:
                prepared_data[keys[0]] = []
            prepared_data[keys[0]].append(line)
        for key in prepared_data:
            group = prepared_data[key]
            train_limit = math.ceil(len(group) * train_size)
            index = 0
            for line in group:
                if index < train_limit:
                    train_data.append(line)
                else:
                    for c in line[class_attribute]:
                        test_labels.append(c)
                    del line[class_attribute]
                    test_data.append(line)
                index += 1
        return train_data, test_data, test_labels
    except FileNotFoundError:
        print(f"Error: The file '{input}' does not exist")


def train_test_split_save(file_name, class_attribute, train_size, random_state=None, overrride=False):
    file_exists = path.exists(file_name+"_train.json")
    if overrride:
        file_exists = False
    if not file_exists:
        x_train, x_test, y_test = train_test_split(file_name+".json", class_attribute, train_size, random_state)
        with open(file_name+"_train.json", 'w') as file:
            json.dump(x_train, file)
        with open(file_name+"_test.json", 'w') as file:
            json.dump(x_test, file)
        with open(file_name+"_labels.json", 'w') as file:
            json.dump(y_test, file)


def create_subgroups(n, iterable):
    args = [iter(iterable)] * n
    return ([e for e in t if e is not None] for t in itertools.zip_longest(*args))


def cross_validation(data, n_splits=5, random_state=None):
    if random_state is None:
        random_state = 0
    for i in range(0, random_state):
        random.shuffle(data)
    n_lists = math.ceil(len(data) / n_splits)
    splits = list(create_subgroups(n_lists, data))
    result = []
    n_splits = len(splits)
    for i in range(0, n_splits):
        class_attribute = str(len(splits[i][0]))
        train_data_raw = splits.copy()
        del train_data_raw[i]
        train_data = []
        for td in train_data_raw:
            train_data += td
        line = {"train_data": train_data}
        test_labels = []
        test_data = []
        for l in splits[i]:
            for c in l[class_attribute]:
                test_labels.append(c)
            tmp = l.copy()
            del tmp[class_attribute]
            test_data.append(tmp)
        line["test_data"] = test_data
        line["test_labels"] = test_labels
        result.append(line)
    return result

