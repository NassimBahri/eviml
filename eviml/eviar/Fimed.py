from eviml.eviar.RidLists import RidLists, Item
import math


class Fimed:
    """
    Generates Evidential Frequent Itemsets and Association Rules

    The FIMED class allows discovering frequent itemsets from an evidential database
    Reference: Bach Tobji, MA, B. Ben Yaghlane, and Khaled Mellouli.
    "A new algorithm for mining frequent itemsets from evidential databases."
    Proceedings of IPMU. Vol. 8. 2008.

    Attributes
    ----------
    frequent_itemsets : list
        List of frequent itemsets
    rules : list
        List of rules
    min_support : float
        Minimum support
    min_confidence : float
        Minimum confidence

    Methods
    -------
    __init__(records, min_support, min_confidence)
        Constructor of the class
    fim()
        Generate the frequent itemsets from the RidLists structure
    bel(itemset)
        Calculate the support of an itemset
    intersection(rids1, rids2)
        Calculate the intersection between the rids of two itemsets
    generate_itemsets(item1, item2, level)
        Generate a candidate itemset from two frequent itemsets
    """

    def __init__(self, records, min_support, min_confidence):
        """Constructor of the class

        Parameters
        ----------
        records : list
            A records iterable object (json format)
        min_support : float
            Minimum support
        min_confidence : float
            Minimum confidence
        """
        self.frequent_itemsets = []
        self.rules = []
        self.min_support = min_support
        self.min_confidence = min_confidence
        self.ridlists = RidLists(records)
        self.fim()
        self.generate_rules()

    def fim(self):
        """Generates the frequent itemsets from the RidLists structure
        """
        k = 0
        self.frequent_itemsets.append({})
        # Generate frequent items (frequent 1-itemsets)
        for item in self.ridlists.rid:
            support = self.bel(item) / self.ridlists.db_size
            if support >= self.min_support:
                self.frequent_itemsets[k][item] = support
        # Generate (k+1)-frequent itemsets
        while len(self.frequent_itemsets[k]) > 0:
            frequent = self.frequent_itemsets[k]
            frequent_itemsets_size = len(frequent)
            candidate_itemsets = []
            step1 = 0
            for item1 in frequent:
                step2 = 0
                for item2 in frequent:
                    if step1 <= step2:
                        continue
                    itemset = Fimed.generate_itemsets(item1, item2, k + 2)
                    if itemset != "":
                        candidate_itemsets.append(itemset)
                    step2 += 1
                step1 += 1
            k += 1
            self.frequent_itemsets.append({})
            for candidate in candidate_itemsets:
                support = self.bel(candidate) / self.ridlists.db_size
                if support >= self.min_support:
                    self.frequent_itemsets[k][candidate] = support

    def bel(self, itemset):
        """Calculate the support of an itemset

        Parameters
        ----------
        itemset : str
            Candidate itemset

        Returns
        -------
        float
            Support of the candidate itemset
        """
        items = itemset.split(";")
        bel = 0
        if len(items) == 1:
            temp = self.ridlists.rid[itemset]
        else:
            temp = self.ridlists.rid[items[0]]
            for i in range(1, len(items)):
                temp = Fimed.intersection(temp, self.ridlists.rid[items[i]])
        for item in temp:
            bel += item.bel
        return bel

    @staticmethod
    def intersection(rids1, rids2):
        """Calculate the intersection between the rids of two itemsets

        Parameters
        ----------
        rids1 : list
            List 1 of triplets (rowid, mass, bel)
        rids2 : list
            List 2 of triplets (rowid, mass, bel)

        Returns
        -------
        list
            List of triplets that represent the intersection
        """
        temp = []
        for item1 in rids1:
            for item2 in rids2:
                if item1.rowid == item2.rowid:
                    temp.append(Item(item1.rowid, item1.mass, item1.bel * item2.bel))
        return temp

    @staticmethod
    def generate_itemsets(item1, item2, level):
        """Generate a candidate itemset from two frequent itemsets

        Parameters
        ----------
        item1 : str
            Frequent itemset 1
        item2 : str
            Frequent itemset 2
        level : int
            Level

        Returns
        -------
        str
            Candidate itemset
        """
        itemset = ""
        equals = True
        values1 = item1.split("#")
        values2 = item2.split("#")
        if values1[0] == values2[0]:
            return ""
        if level == 2:
            items = sorted([item1, item2])
            return ";".join(items)
        else:
            items1 = set(item1.split(";"))
            items2 = set(item2.split(";"))
            items = items1 | items2
            items = sorted(list(items))
            if len(items) == level:
                itemset = ";".join(items)
        return itemset

    @staticmethod
    def per(rule_conf, n):
        """Calculates the Pessimistic Error Rate of a given rule

        Parameters
        ----------
        rule_conf : float
            Confedence of the rule
        n : int
            Number

        Returns
        -------
        float
            Calculated PER
        """
        r = float(1 - rule_conf)
        z = 0.69
        return (r + (math.pow(z, 2) / (2 * n)) + z * (r / n - math.pow(r, 2) / n + math.pow(z, 2) / (4 * n))) / (
                1 + math.pow(z, 2) / n)

    def generate_rules(self):
        """Generates evidential association rules


        Returns
        -------
        list
            List of rules
        """
        if len(self.frequent_itemsets) < 2:
            exit()
        max_level = len(self.frequent_itemsets)
        for level in range(1, max_level):
            frequents = self.frequent_itemsets[level]
            for item, support in frequents.items():
                parts = item.split(";")
                for part in parts:
                    rule_consequent = part
                    antecedent = parts.copy()
                    antecedent.remove(part)
                    rule_antecedent = ";"
                    rule_antecedent = rule_antecedent.join(antecedent)
                    support_antecedent = self.frequent_itemsets[len(antecedent) - 1][rule_antecedent]
                    support_consequent = self.frequent_itemsets[0][rule_consequent]
                    lift = support / (support_antecedent * support_consequent)
                    confidence = support / support_antecedent
                    if confidence >= self.min_confidence:  # and lift > 1 and per < 0.5:
                        self.rules.append({
                            "antecedent": rule_antecedent,
                            "consequent": rule_consequent,
                            "support": support,
                            "confidence": confidence,
                            "lift": lift
                        })
