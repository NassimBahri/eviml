from eviml.evirc.AssociativeClassifier import AssociativeClassifier
import itertools
import math


class DsARM(AssociativeClassifier):

    def __init__(self, rules, class_attribute, class_domain, distance_threshold=2):
        super().__init__(rules, class_attribute)
        # Compute the discount factor
        for rule in self.classification_rules:
            rule["discount_factor"] = 1 / (1 + math.log(DsARM.rule_antecedent_len(rule["antecedent"]), 2))
        self.class_domain = class_domain
        self.class_attribute = class_attribute
        self.distance_threshold = distance_threshold
        self.frequent_label = self.frequent_class_label()

    @staticmethod
    def rule_antecedent_len(antecedent):
        return len(antecedent.split(";"))

    def classify(self, instance):
        focal_elements = DsARM.focal_elements(instance)
        vacuous = ",".join(self.class_domain)
        bbas = []
        final_bba = {}
        #focal_elements = [{'bba': ('1#2', '2#2', '3#2'), 'mass': 0.12}]   # to delete next
        #focal_elements = [{'bba': ('1#2,3', '2#2', '3#2,3'), 'mass': 0.12}]   # to delete next
        for element in focal_elements:
            rules = self.select_matching_rules(element)
            bbas.append(DsARM.rule_combination(rules, vacuous, element["mass"]))
        if len(bbas) == 0:
            self.unclassified_instances += 1
            return self.frequent_label.replace(self.class_attribute + "#", "")
        for bba in bbas:
            for fe in bba:
                if fe not in final_bba:
                    final_bba[fe] = 0
                final_bba[fe] += bba[fe]
        values = {}
        for element in self.class_domain:
            values[element] = round(DsARM.pignistic(set(element), final_bba), 3)
        values = sorted(values.items(), key=lambda item: item[1], reverse=True)
        return values[0][0]

    @staticmethod
    def focal_elements(instance):
        elements = []
        elementsMasses = {}
        for attribute in instance:
            items = []
            for fe, mass in instance[attribute].items():
                items.append(attribute+"#"+fe)
                elementsMasses[attribute+"#"+fe] = mass
            elements.append(items)
        result = []
        for element in itertools.product(*elements):
            # calculate the mass of the generated bba
            mass = 1
            for elem in element:
                mass *= elementsMasses[elem]
            result.append({"bba": element, "mass": round(mass, 2)})
        return result

    def select_matching_rules(self, bba):
        selected_rules = []
        instance_attributes = {}
        for attribute in bba["bba"]:
            data = attribute.split("#")
            instance_attributes[data[0]] = set(data[1].split(","))
        # Tests the inclusion
        for rule in self.classification_rules:
            if DsARM.superset_matching(instance_attributes, rule):
                selected_rules.append(rule)
        # Tests the intersection
        if len(selected_rules) == 0:
            for rule in self.classification_rules:
                distance = DsARM.intersection_matching(instance_attributes, rule)
                if 0 < distance <= self.distance_threshold:
                    selected_rules.append(rule)
        #print(bba)
        #for r in selected_rules:
        #    print(r)
        #print("--------------------")
        return selected_rules

    @staticmethod
    def superset_matching(instance, rule):
        rule_parts = rule["antecedent"].split(";")
        rule_attributes = {}
        for part in rule_parts:
            data = part.split("#")
            rule_attributes[data[0]] = set(data[1].split(","))
        subset = 0
        for attribute, fe in rule_attributes.items():
            if instance[attribute] <= fe:
                subset += 1
        if len(rule_attributes) == subset:
            return True
        return False

    @staticmethod
    def intersection_matching(instance, rule):
        rule_parts = rule["antecedent"].split(";")
        rule_attributes = {}
        distance = 0
        for part in rule_parts:
            data = part.split("#")
            rule_attributes[data[0]] = set(data[1].split(","))
        for attribute, fe in rule_attributes.items():
            if len(instance[attribute] & fe) == 0:
                distance = 0
                break
            d = len(instance[attribute] - fe) + len(fe - instance[attribute])
            distance += len(instance[attribute] - fe) + len(fe - instance[attribute])
        return distance / 2

    @staticmethod
    def rule_combination(rules, vacuous, mass):
        # Transform rules into bbas
        bbas = []
        for rule in rules:
            rule_reliability = rule["confidence"] * rule["discount_factor"]
            bbas.append({
                rule["consequent"].split("#")[1]: round(rule_reliability, 2),
                vacuous: round(1 - rule_reliability, 2)
            })
        final_bba = bbas[0]
        lenght = len(bbas)
        for i in range(1, lenght):
            final_bba = DsARM.dempster_combination(final_bba, bbas[i])
        for fe in final_bba:
            final_bba[fe] = round(final_bba[fe] * mass, 2)
        return final_bba
