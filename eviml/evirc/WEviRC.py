import math

from eviml.evirc.AssociativeClassifier import AssociativeClassifier


class WEviRC(AssociativeClassifier):

    def __init__(self, rules, class_attribute, class_domain, features_domain):
        valid_rules = []
        size = 0
        for feature, domain in features_domain.items():
            size += len(domain)
        super().__init__(rules, class_attribute)
        for rule in self.classification_rules:
            # Compute the IC (Specifity)
            rule["ic"] = round(1 - math.log(WEviRC.rule_antecedent_len(rule["antecedent"]), 10) / size, 3)
            # Compute the ABS (Level of Abstraction)
            rule["loa"] = WEviRC.abstraction_level(rule, features_domain)
            if rule["lift"] > 1:
                valid_rules.append(rule)
        self.classification_rules = valid_rules
        self.class_domain = class_domain
        self.class_attribute = class_attribute
        self.classification_rules = WEviRC.filter_rules(self.classification_rules)
        self.frequent_label = self.frequent_class_label()

    @staticmethod
    def rule_antecedent_len(antecedent):
        size = 0
        rule_features = antecedent.split(";")
        for feature in rule_features:
            data = feature.split("#")
            size += len(data[1].split(","))
        return size

    @staticmethod
    def filter_rules(rules):
        rules = sorted(rules, key=lambda rule: (rule["confidence"], rule["support"]), reverse=True)
        i = 0
        while i < len(rules):
            j = i + 1
            while j < len(rules):
                rule1 = rules[i]
                rule2 = rules[j]
                go_next_step = True
                attributes1 = rule1["antecedent"].split(";")
                attributes2 = rule2["antecedent"].split(";")
                # attribute2 includes attribute1
                is_subset = all(attribute in attributes2 for attribute in attributes1)
                if (is_subset and rule1["confidence"] > rule2["confidence"]) or \
                        (is_subset and rule1["confidence"] == rule2["confidence"] and rule1["support"] > rule2["support"]):
                    go_next_step = False
                    del rules[j]
                if go_next_step:
                    j += 1
            i += 1
        return rules

    def classify(self, instance):
        instance_attributes = {}
        instance_data = {}
        instance_masses = {}
        instance_focal_elements = []
        for attr in instance:
            instance_attributes[attr] = []
            instance_data[attr] = []
            instance_masses[attr] = []
            for focal_element in instance[attr]:
                instance_attributes[attr].append(attr + "#" + focal_element)
                instance_data[attr].append({"focal_element": focal_element, "mass": instance[attr][focal_element]})
                instance_masses[attr].append(instance[attr][focal_element])
                instance_focal_elements.append(attr + "#" + focal_element)
        rules = self.classification_rules
        i = 0
        while i < len(rules):
            rule_parts = rules[i]["antecedent"].split(";")
            rule_mass = 1
            total_intersections = 0
            for rule_part in rule_parts:
                attribute_mass = 0
                elements = rule_part.split("#")
                rule_focal_elements = set(elements[1].split(","))
                instance_values = []
                if elements[0] in instance_data:
                    instance_values = instance_data[elements[0]]
                for fe in instance_values:
                    values = set(fe["focal_element"].split(","))
                    intersection = len(values & rule_focal_elements)
                    union = len(values | rule_focal_elements)
                    if intersection > 0:
                        attribute_mass += fe["mass"] * intersection / union
                if attribute_mass > 0:
                    total_intersections += 1
                rule_mass *= attribute_mass
            if total_intersections == len(rule_parts):
                rules[i]["reliability"] = rule_mass
                i += 1
            else:
                del rules[i]
        if len(rules) == 0:
            self.unclassified_instances += 1
            return self.frequent_label.replace(self.class_attribute + "#", "")
        bbas = []
        # for r in rules:
        #    print(r)
        for rule in rules:
            contents = rule["consequent"].split("#")
            rule_class = contents[1]
            domain = self.class_domain.copy()
            domain.remove(rule_class)
            other = ",".join(domain)
            rule_reliability = rule["confidence"] * rule["reliability"] * rule["ic"]
            other_reliability = (1 - rule["confidence"]) * rule["reliability"] * rule["ic"]
            bbas.append({
                rule_class: round(rule_reliability, 2),
                other: round(other_reliability, 2),
                ",".join(self.class_domain): round(1 - rule_reliability - other_reliability, 2)
            })
        final_bba = bbas[0]
        lenght = len(bbas)
        for i in range(1, lenght):
            final_bba = WEviRC.dempster_combination(final_bba, bbas[i])
        values = {}
        for element in self.class_domain:
            values[element] = round(WEviRC.pignistic(set(element), final_bba), 3)
        values = sorted(values.items(), key=lambda item: item[1], reverse=True)
        return values[0][0]

    @staticmethod
    def abstraction_level(rule, features_domain):
        rule_parts = rule["antecedent"].split(";")
        loa = 0
        for part in rule_parts:
            values = part.split("#")
            focal_elements = set(values[1].split(","))
            if set(features_domain[values[0]]) == focal_elements:
                loa += 1
        return 1 - (loa / len(features_domain))
