"""Metrics to assess the performance of the classifier given the correct and the predicted class labels.
"""

# Authors: Nassim Bahri <bahri.nassim@gmail.com>
#          Mohamed Anis Bach Tobji <anis.bach@esen.tn>
#          Boutheina Ben Yaghlane <olivier.grisel@ensta.org>
# License: MIT


def accuracy(y_true, y_pred):
    """Accuracy classification

    This function computes the accuracy of the classifier. The accuracy is the ratio ``TP / n`` where ``TP`` is the number
    of correctly predicted classes and ``n`` is the number of instances.

    Parameters
    ----------
    y_true : {list}
        Real (correct) labels
    y_pred : {list}
        Predicted labels, as returned by a classifier

    Returns
    -------
    float
        The fraction of correctly classified samples

    Examples
    --------
    >>> from eviml.metrics import accuracy
    >>> y_true = [0, 1, 2, 0, 1, 2]
    >>> y_pred = [0, 2, 1, 0, 0, 1]
    >>> print(accuracy(y_true, y_pred))
    >>> 0.3333333333333333
    """
    size = len(y_true)
    if size != len(y_pred):
        print("Accuracy: labels size error")
        return 0
    correct = 0
    for i in range(0, size):
        if y_true[i] == y_pred[i]:
            correct += 1
    return correct / size


def precision(y_true, y_pred):
    """Computes the precision

    The precision is the ratio ``tp / (tp + fp)`` where ``tp`` is the number of true positives and ``fp`` the number of false positives.
    For the case of multi-label classification, the precision is equal to: ``Precision(C1) = CP(C1) / P(C1)`` where ``CP(C1)``
    is the number of correctly predicted C1 and ``P(C1)`` refers to all predicted C1.

    Parameters
    ----------
    y_true : list
        Real (correct) labels
    y_pred : list
        Predicted labels, as returned by a classifier

    Returns
    -------
    dict
        Array containing the precision of each class label. The key is the class label and the value
        is its precision.

    Examples
    --------
    >>> from eviml.metrics import precision
    >>> y_true = [0, 1, 2, 0, 1, 2]
    >>> y_pred = [0, 2, 1, 0, 0, 1]
    >>> print(precision(y_true, y_pred))
    >>> {0: 0.6666666666666666, 1: 0.0, 2: 0.0}
    """
    class_labels = {}
    for indice, label in enumerate(y_pred):
        if label not in class_labels:
            class_labels[label] = {"true": 0, "predicted": 0}
        if y_true[indice] not in class_labels:
            class_labels[y_true[indice]] = {"true": 0, "predicted": 0}
        class_labels[label]["predicted"] += 1
        if label == y_true[indice]:
            class_labels[label]["true"] += 1
    precisions = {}
    class_labels_sorted = sorted(class_labels)
    for label in class_labels_sorted:
        data = class_labels[label]
        precisions[label] = data["true"] / data["predicted"]
    return precisions


def recall(y_true, y_pred):
    """Computes the recall

    The recall is the ratio ``tp / (tp + fn)`` where ``tp`` is the number of true positives and ``fn`` the number of false negatives.
    For the case of multi-label classification, the recall is equal to: ``Recall(C1) = CP(C1) / A(C1)`` where ``CP(C1)``
    is the number of correctly predicted C1 and ``A(C1)`` refers to the number of actual C1.

    Parameters
    ----------
    y_true : list
        Real (correct) labels
    y_pred : list
        Predicted labels, as returned by a classifier

    Returns
    -------
    dict
        Array containing the recall of each class label. The key is the class label and the value
        is its recall.

    Examples
    --------
    >>> from eviml.metrics import recall
    >>> y_true = [0, 1, 2, 0, 1, 2]
    >>> y_pred = [0, 2, 1, 0, 0, 1]
    >>> print(recall(y_true, y_pred))
    >>> {0: 1.0, 1: 0.0, 2: 0.0}
    """
    class_labels = {}
    for indice, label in enumerate(y_true):
        if label not in class_labels:
            class_labels[label] = {"true": 0, "total": 0}
        if y_pred[indice] not in class_labels:
            class_labels[y_true[indice]] = {"true": 0, "total": 0}
        class_labels[label]["total"] += 1
        if label == y_pred[indice]:
            class_labels[label]["true"] += 1
    recalls = {}
    class_labels_sorted = sorted(class_labels)
    for label in class_labels_sorted:
        data = class_labels[label]
        recalls[label] = data["true"] / data["total"]
    return recalls


def f1_score(y_true, y_pred):
    """Computes the F1 score, also known as balanced F-score or F-measure

    The F1 score can be interpreted as a weighted average of the precision and recall. The formula for the F1 score is:
    ``F1 = 2 * (precision * recall) / (precision + recall)``.

    Parameters
    ----------
    y_true : {list}
        Real (correct) labels
    y_pred : {list}
        Predicted labels, as returned by a classifier

    Returns
    -------
    dict
        Array containing the F1 score of each class label. The key is the class label and the value
        is its F1 score.

    See Also
    --------
    recall, precision

    Examples
    --------
    >>> from eviml.metrics import f1_score
    >>> y_true = [0, 1, 2, 0, 1, 2]
    >>> y_pred = [0, 2, 1, 0, 0, 1]
    >>> print(f1_score(y_true, y_pred))
    >>> {0: 0.8, 1: 0, 2: 0}
    """
    recalls = recall(y_true, y_pred)
    precisions = precision(y_true, y_pred)
    f1_scores = {}
    for label, _recall in recalls.items():
        _precision = precisions[label]
        if _recall == _precision == 0:
            f1 = 0
        else:
            f1 = 2 * (_precision * _recall) / (_precision + _recall)
        f1_scores[label] = f1
    return f1_scores
