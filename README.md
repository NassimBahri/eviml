![](https://www.nassimbahri.ovh/static/images/logo-eviml.png)

-----------------

# EviML

EviML is a Machine Learning Library for dealing with Evidential Data.

## Main Features
This library includes:

  - Loading JSON files,
  - Frequent itemsets mining,...

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install EviML.

```bash
pip install eviml
```

## Usage

```python
import eviml

# examples will be available soon
```

## Documentation
The official documentation is hosted on: nassimbahri.ovh/eviml

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Authors
* [Nassim Bahri](https://www.nassimbahri.ovh) - *PhD Student in Business Computing*
* [Mohamed Anis Bach Tobji](https://www.esen.tn/portail/enseignant/bachtobji) - *Associate Professor in Business Computing*
* [Boutheina Ben Yaghlane](http://www.larodec.com/member/conseil_scientifique/ben-yaghlane-boutheina) - *Professor in Business Computing*


## License
This project is licensed under the [MIT](https://choosealicense.com/licenses/mit/) License
